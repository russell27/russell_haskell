import System.Environment
import System.IO
import System.Console.GetOpt
import CSV
import Stack
import Text.Parsec.Error (ParseError)
import Control.Monad
import Data.List
import Data.List.Split

version :: Double
version = 0.1

data Options = Options 
    { optHelp         :: Bool
    , optVersion      :: Bool
    , optStack        :: Bool
    , optGroups       :: Bool
    , optGName        :: Bool
    , optDefaultGroup :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp         = False
    , optVersion      = False
    , optStack        = True
    , optGroups       = False
    , optGName        = False
    , optDefaultGroup  = False
    }



usage :: String
usage = "Usage: stack exec -- csvstack [OPTION...] fileName1 fileName2 .."

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version" 
    , Option "s"  ["stack"] 
                (NoArg (\o -> o { optStack = True }))
                "stacks up given csv files"
    , Option "g"  ["group"] 
                (NoArg (\o -> o { optGroups = True }))
                 "group names for each file"
     , Option "n"  ["gname"]
                (NoArg (\o -> o { optGName = True }))
                 "rename the 'Group' column name to given name, works only when -g is used"
     , Option "d"  ["filenames"]
                (NoArg (\o -> o { optDefaultGroup = True }))
                 "name the groups with file names"
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help"] 

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))
              
parseData :: String -> [[String]]
parseData dat = case parseCSV dat of 
    Right result -> result

processFile :: [String] -> IO [String]
processFile args = do
      firstFileHandle <- if head args == "-" then getContents    else readFile (head args)
      remFiles <- sequence [if x == "-" then getContents else  readFile x | x <- tail args]
      return [intercalate "," csv | csv <- concat $ parseData firstFileHandle : [ tail csv | csv <- map parseData remFiles]]

determineGroupName :: String -> String
determineGroupName groupName
    | groupName == "" = "Group"
    | otherwise               = groupName

addGroups :: [String] -> String -> IO [String]
addGroups args groupName = do
      stackedCSV <- processFile fileList
      fileLengths <- mapM countLinesInFile fileList
      let recordsList = [take x (tail stackedCSV) | x <- fileLengths] 
      let name = determineGroupName groupName
      let outCSV = (name ++ "," ++ head stackedCSV) : concat [[group ++ "," ++ record | record <- records] | group <- groups, records <- recordsList]
      return outCSV
      where
      fileList = take (length groups) $ drop (length groups) args
      groups = take (length args `div` 2) args

parseFilePath :: String -> String
parseFilePath path = head $ reverse $ splitOn "/" path
 
addGroupsAsFilenames :: [String] -> String -> IO [String]
addGroupsAsFilenames args groupName = do
        stackedCSV <- processFile fileList
        fileLengths <- mapM countLinesInFile fileList
        let recordsList = [take x (tail stackedCSV) | x <- fileLengths]
        let name = determineGroupName groupName
        let outCSV = (name ++ "," ++ head stackedCSV) : concat [[group ++ "," ++ record | record <- records] | group <- groups, records <- recordsList]
        return outCSV
        where
        fileList = args
        groups = [parseFilePath x | x <- fileList]


showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "csvstack version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts
        then showInfo opts 
     else if optGroups opts  && optGName opts
         then do 
            out <- addGroups (tail arguments) (head arguments)
            putStrLn $ unlines out
     else if optGroups opts  && even (length arguments) 
        then do
            out <- addGroups arguments ""
            putStrLn $ unlines out
    else if optDefaultGroup opts  && optGName opts
         then do
            out <- addGroupsAsFilenames (tail arguments) (head arguments)
            putStrLn $ unlines out
    else if optDefaultGroup opts
        then do
            out <- addGroupsAsFilenames arguments ""
            putStrLn $ unlines out
    else if optStack opts
        then do
              out <- processFile arguments
              putStrLn $ unlines out
    else putStrLn "Give Valid Options."
