import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import CSV
import Look
import Table


version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optLookTool    :: Look
    , optMaxRows     :: Bool
    , optMaxCols     :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optLookTool  = Table.unicodeWithoutHeader
    , optMaxRows   = False
    , optMaxCols   = False
    }

lookOptionalArgs :: [Look]
lookOptionalArgs = 
            [Table.unicodeTable
            , Table.unicodeWithoutHeader
            , Table.asciiTable
            , Table.asciiWithoutHeader
            , Table.unicodeWithNumbering
            ]

usage :: String
usage = "Usage: stack exec -- csvlook [OPTION...] fileName"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "r"  ["trunc-rows"]
        (NoArg (\o -> o { optMaxRows = True }))
        "Prints truncated table with maximum number of rows as specified"
    , Option "c"  ["trunc-columns"]
        (NoArg (\o -> o { optMaxCols = True }))
        "Prints truncated table with maximum number of columns as specified"
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show help"] ++ moreOptions
        where
        moreOptions = map converterToOption lookOptionalArgs
        converterToOption c@(Look name _ short long) = 
            Option short [long] 
                (NoArg (\o -> o {optLookTool = c}))
                ("Prints " ++ name)


getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))

processOpts :: Table -> Options -> String
processOpts table  (Options _ _ Look { cLook = f } _ _ ) = f table
                
parseData :: String -> IO Table
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result


processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  head args == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    putStrLn (processOpts table  opts)

showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "csvlook version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"

printTruncatedRows :: Options -> [String] -> IO ()
printTruncatedRows opts args = do
    fileData <- if  last args == "-" then getContents    else readFile (last args)
    table <- parseData fileData
    let n = read (head args) :: Int
    putStrLn $ unicodeMaxRows table n

printTruncatedColumns :: Options -> [String] -> IO ()
printTruncatedColumns opts args = do
    fileData <- if  last args == "-" then getContents    else readFile (last args)
    table <- parseData fileData
    let n = read (head args) :: Int
    putStrLn $ unicodeMaxColumns table n

--main :: Options
main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
        then showInfo opts
    else if optMaxRows opts
        then printTruncatedRows opts arguments
    else if optMaxCols opts
        then printTruncatedColumns opts arguments
    else processFile opts arguments

