{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
 
import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import Data.Char
import Data.Function
import CSV
import CleanOptions

version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optCheck       :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optCheck     = True
    }

usage :: String
usage = "Usage: stack exec -- csvclean [OPTION...] fileName"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "n"  ["dry-run"] 
        (NoArg (\o -> o { optCheck = True }))
        "Do not create output files. Information about what would have been done will be printed to STDERR."
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help message and exit."]

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError
                            (concat errors ++ usageInfo usage options))

parseData :: String -> IO [[String]]
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result

processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (getErrors table == []) then putStrLn ("No errors") else (putStrLn $ unlines $ format (getErrors table) table)

showInfo :: Options -> IO ()
showInfo Options { optVersion = True } = putStrLn $ "csvclean version " ++ show version
showInfo Options { optHelp = True }    = putStr $ usageInfo usage options
showInfo _                             = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
       then showInfo opts
    else (if (optCheck opts && (length arguments > 0)) then (processFile opts arguments) else putStrLn ("Error: Arguments not given"))
