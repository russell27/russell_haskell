{-# LANGUAGE OverloadedStrings #-}
import System.Environment
import System.IO
import System.Console.GetOpt
import CSV
import SQL2CSV
import Text.Parsec.Error (ParseError)
import Control.Monad
import Data.List
import Database.HDBC
import Database.HDBC.Sqlite3


version :: Double
version = 0.1

data Options = Options 
    { optHelp         :: Bool
    , optVersion      :: Bool
    , optdbString     :: Bool
    , optQuery        :: Bool
    , optUser         :: Bool
    , optPwd          :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp           = False
    , optVersion        = False
    , optdbString       = False
    , optQuery          = False
    , optUser           = False
    , optPwd            = False
    }

usage :: String
usage = "Usage: sql_csv --db dbString --query "

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version" 
    , Option ""  ["query"] 
                (NoArg (\o -> o { optQuery = True }))
                "sqlite select query to be executed"
    ,Option ""  ["db"]
                (NoArg (\o -> o { optdbString = True }))
                "database connection url"
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help"]

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))
              
parseData :: String -> [[String]]
parseData dat = case parseCSV dat of 
    Right result -> result



parseOpts :: [String] -> IO()
parseOpts inputs = toCSV (inputs !! 0) (inputs !! 1)



showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "csvsql version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"


main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts
        then showInfo opts
    else if optdbString opts && optQuery opts
        then parseOpts arguments
    
    else putStrLn "Give Valid Options."
