{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
 
import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import CSV
import Cut
import CutOptional

version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optCutTool     :: Cut
    , optGetHeader   :: Bool
    , optEmptyrows   :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optCutTool  = CutOptional.columns
    , optGetHeader = False
    , optEmptyrows = False
    }

cutOptionalArgs :: [Cut]
cutOptionalArgs = 
            [CutOptional.columns
            ,CutOptional.not_columns
            ]

usage :: String
usage = "Usage: stack exec -- csvcut [OPTION...] fileName pattern"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "n"  ["names"]
        (NoArg (\o -> o { optGetHeader = True }))
        "Display column names and indices from the input CSV and exit."
    , Option "x"  ["delete-empty-rows"] (NoArg (\o -> o { optEmptyrows = True
}))
        "Removes Empty rows in CSV File." 
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help"] ++ moreOptions
        where
        moreOptions = map converterToOption cutOptionalArgs
        converterToOption c@(Cut name _ short long) = 
            Option short [long] 
                (NoArg (\o -> o {optCutTool = c}))
                ("Does " ++ name)


getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))

processOpts :: Table -> String -> Options -> Table
processOpts table text (Options _ _ (Cut { cCut = f }) _ _ ) = f table text
                
parseData :: String -> IO Table
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result


processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  (head args) == "-" then getContents    else readFile (last args)
    table <- parseData fileData
    putStr $ unlines [intercalate "," y | y <- transpose(processOpts table (head args) opts)]

processFileH :: Options -> [String] -> IO ()
processFileH opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (last args)
    table <- parseData fileData
    putStrLn $ unlines $  map (\ x -> (fst x) ++ ":" ++ (snd x)) (zip (map show [1..]) (head table))

processFileX :: Options -> [String]-> IO ()
processFileX opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (last args)
    table <- parseData fileData
    putStr $ unlines [intercalate "," y | y <- filter (\e -> e/=[""]) table]

showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "tablify version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
        then showInfo opts
        else (
            if ( (length arguments) > 1) 
                then (processFile opts arguments) 
                else (if (optGetHeader opts)
                        then ( processFileH opts arguments)
                        else (if (optEmptyrows opts)
                        then ( processFileX opts arguments)
                        else putStrLn ("Error: Arguments not given"))))
