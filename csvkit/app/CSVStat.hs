{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
 
import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import Data.Char
import Data.Function
import CSV
import StatOptions
import Stat

version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optStatTool    :: Stat
    , optGetHeader   :: Bool
    , optFreq        :: Bool
    , optCols        :: Bool
    , optNulls       :: Bool
    , optRowCount    :: Bool
    , optType        :: Bool
    , optLonglen     :: Bool
    , optUnique      :: Bool
    , optStdev       :: Bool
    , optFreqCount   :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optStatTool  = StatOptions.mins
    , optGetHeader = False
    , optFreq      = False
    , optCols      = False
    , optNulls     = False
    , optRowCount  = False
    , optType      = False
    , optLonglen   = False
    , optUnique    = False
    , optStdev     = False
    , optFreqCount = False
    }

statOptionalArgs :: [Stat]
statOptionalArgs = 
            [StatOptions.mins
            , StatOptions.maxs
            , StatOptions.sums
            , StatOptions.means
            , StatOptions.medians
            ]

usage :: String
usage = "Usage: stack exec -- csvstat [OPTION...] fileName columns"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "n"  ["names"]
        (NoArg (\o -> o { optGetHeader = True }))
        "Display column names and indices from the input CSV and exit."
    , Option "t"  ["type"]
        (NoArg (\o -> o { optType = True }))
        "Only output data type."
    , Option "r"  ["count"]
        (NoArg (\o -> o { optRowCount = True }))
        "Only output total row count."
    , Option "c"  ["columns"]
        (NoArg (\o -> o { optCols = True }))
        "A comma separated list of column indices or names to be examined. Defaults to all columns."
    , Option "N"  ["nulls"]
        (NoArg (\o -> o { optNulls = True }))
        "Only output whether columns contains nulls."
    , Option "f"  ["freq"]
        (NoArg (\o -> o { optFreq = True }))
        "Only output lists of frequent values."
    , Option "q"  ["freq-count"]
        (NoArg (\o -> o { optFreqCount = True }))
        "The maximum number of frequent values to display."
     , Option "u"  ["unique"]
        (NoArg (\o -> o { optUnique = True }))
        "Only output counts of unique values."
    , Option "l"  ["len"]
        (NoArg (\o -> o { optLonglen = True }))
        "Only output the length of the longest values."
     , Option "z"  ["stdev"]
        (NoArg (\o -> o { optStdev = True }))
        "Only output standard deviations."   
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help message and exit"] ++ moreOptions
        where
        moreOptions = map converterToOption statOptionalArgs
        converterToOption c@(Stat name _ short long) = 
            Option short [long] 
                (NoArg (\o -> o {optStatTool = c}))
                ("Only output " ++ name ++ "s.")


getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))

processOpts :: Table -> Options -> [Int]
processOpts table (Options _ _ (Stat { sStat = f }) _ _ _ _ _ _ _ _ _ _) = f table
                
parseData :: String -> IO Table
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result

getRowCount :: Options -> [String] -> IO ()
getRowCount opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    putStrLn $ "Row Count :" ++ rowCount table

processFileH :: Options -> [String] -> IO ()
processFileH opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    putStrLn $ unlines $  map (\ x -> (fst x) ++ ":" ++ (snd x)) (zip (map show [1..]) (head table))

processFile opts args f = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (length args) == 1 then (putStrLn $ unlines $ applyFunction f table (allCols table)) 
    else (putStrLn $ unlines $ applyFunction f table (last args))

processFileFreqs opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (length args) == 1 then putStrLn $ unlines $ freqAllCols (formatResult $ getFreqs table (allCols table) 5) table
                          else putStrLn $ unlines $ freqGivenCols (formatResult $ getFreqs table (last args) 5) (last args) table

forTexts :: Int -> [String] -> String
forTexts n xs = head xs ++ "\n\n\t Type of Data : " ++ isNumericCol xs ++ "\n\t Contains null values : " ++ (show (isNullCol xs)) ++ "\n\t Unique values : " ++ (show (howManyUnique xs)) ++ "\n\t Longest value : " ++ (show (longestLen xs)) ++ " characters" ++ "\n\t Most common values : " ++ mostCommonValues n xs

forNums :: Int -> [String] -> String
forNums n xs = head xs ++ "\n\n\t Type of Data : " ++ isNumericCol xs ++ "\n\t Contains null values : " ++ (show (isNullCol xs)) ++ "\n\t Unique values : " ++ (show (howManyUnique xs)) ++ "\n\t Smallest value : " ++ (show (minimum (toInts xs))) ++ "\n\t Largest value : " ++ (show (maximum (toInts xs))) ++ "\n\t Sum : " ++ (show (sum (toInts xs))) ++ "\n\t Mean : " ++ (show (mean (toInts xs))) ++ "\n\t Median : " ++ (show (median xs)) ++ "\n\t StDev : " ++ (show (stdev (filter (not.null) xs))) ++ "\n\t Most common values : " ++ mostCommonValues n xs

formatOutput :: Int -> [String] -> String
formatOutput n xs = if (isNumericCol xs) == "Text" then (forTexts n xs) else (forNums n xs)
                      
mostCommonValues :: Int -> [String] -> String
mostCommonValues n xs = concat $ map (\x -> x ++ ['\n'] ++ "\t\t\t" ++ "      ") (take n $ reverse $ formatFreqs (freqElems xs))

cols s n table = putStr $ unlines $ (map (\ x -> formatOutput n x) (rmElems $ rmElems $ setNumsNames $ getCols table s)) ++ ["\nRow Count : " ++ (rowCount table)]

processFileCols opts args = do 
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (length args == 1) then cols (allCols table) 5 table
                          else cols (last args) 5 table

getStdev :: Options -> [String] -> IO ()
getStdev opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (length args) == 1 then (putStr $ unlines $ numstdev table (allCols table))  else (putStr $ unlines $ numstdev table (last args))
    if (length args) == 1 then (putStr $ unlines $ texts table (allCols table)) else (putStrLn $ unlines $ texts table (last args))

nums table cols opts = map (\ x -> (fst x) ++ ": " ++ show (snd x)) (zip (map head tableNum) (processOpts tableNum opts))
                           where tableNum = [filter (not.null) x | x <- (rmElems $ rmElems $ setNumsNames $ getCols table cols), isNumericCol x == "Number"]

processFileNum :: Options -> [String] -> IO ()
processFileNum opts args = do  
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (length args) == 1 then (putStr $ unlines $ nums table (allCols table) opts) else (putStr $ unlines $ nums table (last args) opts)
    if (length args) == 1 then (putStrLn $ unlines $ texts table (allCols table)) else (putStrLn $ unlines $ texts table (last args))



processFileFreqCount :: Options -> [String] -> IO ()
processFileFreqCount opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    if (length args) == 1 then putStrLn $ "Arguments not given" 
                          else (if (length args == 2) then (cols (allCols table) n table) else (cols (last args) n table)) where n = read (args !! 1) :: Int

decide opts arguments | optFreq opts      = processFileFreqs opts arguments
                      | optFreqCount opts = processFileFreqCount opts arguments
                      | optCols opts      = processFileCols opts arguments
                      | optNulls opts     = processFile opts arguments isNullCol
                      | optType opts      = processFile opts arguments isNumericCol
                      | optLonglen opts   = processFile opts arguments longestLen
                      | optUnique opts    = processFile opts arguments howManyUnique
                      | optStdev opts     = getStdev opts arguments
                      | optRowCount opts  = getRowCount opts arguments
                      | optGetHeader opts = processFileH opts arguments
                      | otherwise         = processFileNum opts arguments

showInfo :: Options -> IO ()
showInfo Options { optVersion = True } = putStrLn $ "csvstat version " ++ show version
showInfo Options { optHelp = True }    = putStr $ usageInfo usage options
showInfo _                             = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
       then showInfo opts
    else (if (length arguments >= 1) then (decide opts arguments) else putStrLn ("Error: Arguments not given"))
