{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
 
import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import Data.Char
import Control.Monad
import Data.Function
import CSV
import FormatOptions

version :: Double
version = 0.1

data Options = Options 
    { optHelp          :: Bool
    , optVersion       :: Bool
    , optTabs          :: Bool
    , optDelimiter     :: Bool
    , optTerminator    :: Bool
    , optQuoteChar     :: Bool
    , optNoDoubleQuote :: Bool
    , optQuoting       :: Bool
    , optEscapeChar     :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp          = False
    , optVersion       = False
    , optTabs          = False
    , optDelimiter     = False
    , optTerminator    = False
    , optQuoteChar     = False
    , optNoDoubleQuote = False
    , optQuoting       = False
    , optEscapeChar    = False
    }

usage :: String
usage = "Usage: stack exec -- csvformat [OPTION...] fileName delimiter/terminator/character"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "D"  ["out-delimiter"]
        (NoArg (\o -> o { optDelimiter = True }))
        "Delimiting character of the output CSV file."
    , Option "T"  ["out-tabs"]
        (NoArg (\o -> o { optTabs = True }))
        "Specify that the output CSV file is delimited with tabs. Overrides -D."
    , Option "M"  ["out-terminator"]
        (NoArg (\o -> o { optTerminator = True }))
        "Character used to terminate lines in the output CSV file."
    , Option "Q"  ["out-quotechar"]
        (NoArg (\o -> o { optQuoteChar = True }))
        "Character used to quote strings in the output CSV file."
    , Option "P"  ["out-escapechar"]
        (NoArg (\o -> o { optEscapeChar = True }))
        "Character used to escape the delimiter in the output CSV file if --quoting 3 (Quote None) is specified \nand to escape the QUOTECHAR if --no-doublequote is specified."
    , Option "B"  ["out-no-doublequote"]
        (NoArg (\o -> o { optNoDoubleQuote = True }))
       "Whether or not double quotes are doubled in the output CSV file."
    , Option "U"  ["out-quoting"]
        (NoArg (\o -> o { optQuoting = True }))
       "{0,1,2,3} Quoting style used in the output CSV file. 0 = Quote Minimal, 1 = Quote All, 2 = Quote Non-numeric, 3 = Quote None."
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help message and exit."]

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError
                            (concat errors ++ usageInfo usage options))

parseData :: String -> IO [[String]]
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result

applyEscapeChar :: [[String]] -> IO ()
applyEscapeChar table = forM_ table $ \xs -> mapM_ putStr $ (map (\ x -> if x /= (last xs) then x ++ "," else x) xs) ++ ["\n"]

applyQuotesAll :: [[String]] -> IO ()
applyQuotesAll table = forM_ table $ \xs -> mapM_ putStr $ (map (\ x -> if x /= (show (last xs)) then x ++ "," else x) (map show xs)) ++ ["\n"]

applyQuotes s table | s == "1" || s == "2" = applyQuotesAll table
                    | otherwise            = applyDelimiter "," table

applyDelimiter s table = putStr $ unlines $ setDelimiter s table

apply table opts args | optTabs opts                          = applyDelimiter "\t" table
                      | optTerminator opts && length args > 1 = putStr $ concat  $ setTerminator (last args) table
                      | optDelimiter opts  && length args > 1 = applyDelimiter (last args) table
                      | optQuoteChar opts  && length args > 1 = applyDelimiter "," (quoteChar (last args) table)
                      | optQuoting opts                       = applyQuotes (last args) table     
                      | optEscapeChar opts                    = applyEscapeChar $ map (\ x -> getElems (head (last args)) x) table
                      | optNoDoubleQuote opts || otherwise    = applyDelimiter "," table

processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    apply table opts args

showInfo :: Options -> IO ()
showInfo Options { optVersion = True } = putStrLn $ "csvformat version " ++ show version
showInfo Options { optHelp = True }    = putStr $ usageInfo usage options
showInfo _                             = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
       then showInfo opts
    else (if (length arguments > 0) then (processFile opts arguments) else putStrLn ("Error: Arguments not given"))
