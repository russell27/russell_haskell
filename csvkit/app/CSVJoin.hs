{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
 
import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import CSV
import Join
import JoinOptional

version :: Double
version = 0.1

data Options = Options 
    { optHelp         :: Bool
    , optVersion      :: Bool
    , optdefInner     :: Bool
    , optJoinTool     :: Join
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optdefInner = True
    , optJoinTool  = JoinOptional.inner
    }

joinOptionalArgs :: [Join]
joinOptionalArgs = 
            [JoinOptional.inner
            ,JoinOptional.left
            ,JoinOptional.right
            ]

usage :: String
usage = "Usage: stack exec -- csv_join [OPTION...] pattern  file1Name  file2Name "

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "i"  ["defaultinner"]
        (NoArg (\o -> o { optdefInner = True }))
        "Execute a SQL-like innerjoin to merge CSV files."
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help"] ++ moreOptions
        where
        moreOptions = map converterToOption joinOptionalArgs
        converterToOption c@(Join name _ short long) = 
            Option short [long] 
                (NoArg (\o -> o {optJoinTool = c}))
                ("Does " ++ name)


getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))

processOpts :: Table -> Table -> String -> Options -> Table
processOpts table1 table2 text (Options _ _ _ (Join { jJoin = f })) = f table1 table2 text
                
parseData :: String -> IO Table
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result

defaultInner :: Table -> Table -> Table
defaultInner a b = map concat [[x,y] | (x,y) <- zipWithPadding "" "" a b]

processFile :: Options -> [String] -> IO()
processFile opts args = do
      fileData1 <- if  (last(init args)) == "-" then getContents    else readFile (last(init args))
      table1 <- parseData fileData1
      fileData2 <- if  (last args) == "-" then getContents    else readFile (last args)
      table2 <- parseData fileData2
      putStr $  unlines [intercalate "," y | y <- processOpts table1 table2 (head args) opts]
      
processFileI :: [String] -> IO()
processFileI args = do
      fileData1 <- if  (head args) == "-" then getContents    else readFile (head args)
      table1 <- parseData fileData1
      fileData2 <- if  (last args) == "-" then getContents    else readFile (last args)
      table2 <- parseData fileData2
      putStr $  unlines [intercalate "," y | y <- defaultInner table1 table2 ]

showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "tablify version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
        then showInfo opts
        else
            if length arguments == 2
            then processFileI arguments 
            else
                if length arguments > 2
                then processFile opts arguments
                else putStrLn "Error: Arguments not given" 
