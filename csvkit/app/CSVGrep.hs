import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import CSV
import Grep
import Text
import Regex


version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optGrepTool    :: Grep
    , optGetHeader   :: Bool
    , optGetColumns  :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp       = False
    , optVersion    = False
    , optGrepTool   = Regex.matchR
    , optGetHeader  = False
    , optGetColumns = False
    }

grepOptionalArgs :: [Grep]
grepOptionalArgs = 
            [Text.match
            , Text.reverseMatch
            , Regex.matchR
            ]

usage :: String
usage = "Usage: stack exec -- csvgrep [OPTION...] fileName pattern"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "g"  ["get-header"]
        (NoArg (\o -> o { optGetHeader = True }))
        "gets the header, Usage: [OPTION] fileName"    
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "this is help"
    , Option "c"  ["columns"] 
         (NoArg (\o -> o { optGetColumns = True }))
         "extract the columns based on column indices from CSV"] ++ moreOptions        where
        moreOptions = map converterToOption grepOptionalArgs
        converterToOption c@(Grep name _ short long) = 
            Option short [long] 
                (NoArg (\o -> o {optGrepTool = c}))
                ("Returns " ++ name)


getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))

processOpts :: Table -> String -> Options -> Table
processOpts table text (Options _ _ Grep { cGrep = f } _ _ ) = f table text
                
parseData :: String -> IO Table
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result


processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  head args == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    putStr $  unlines [intercalate "," y | y <- processOpts table (last args) opts]

search :: [Int] -> [String] -> [String]
search js xs = [xs!!j | j <- js]
              
extractColumns :: [String] -> IO ()
extractColumns args = do  
    fileData <- if  head args == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    let outCSV = [search indices row | row <- table]
    putStr $  unlines [intercalate "," y | y <- outCSV]
        where indices = [read arg :: Int | arg <- tail args]

processFileH :: [String] -> IO ()
processFileH args = do
    fileData <- if  head args == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    putStrLn $ unlines $  zipWith (curry (\ x -> fst x ++ ":" ++ snd x)) (map show [1 .. ]) (head table)

showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "tablify version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"

--main :: Options
main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
        then showInfo opts
        else
            if length arguments >= 2 && optGetColumns opts
                then extractColumns arguments
                else if optGetHeader opts
                        then processFileH arguments
                else if length arguments == 2
                        then processFile opts arguments
                        else putStrLn "Error: Arguments not given"
