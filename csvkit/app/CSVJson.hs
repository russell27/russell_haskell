{-# LANGUAGE OverloadedStrings #-}

import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import Data.Char
import Data.Function
import CSV
import Data.Text as T
import Dynamic
import Data.List as L
import JSONOptions

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optKey         :: Bool
    , optIndent      :: Bool
    , optStream      :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optKey       = False
    , optStream    = False 
    , optIndent    = True
    }

version :: Double
version = 0.1

usage :: String
usage = "Usage: stack exec -- csv_json [option] value fileName"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "k"  ["key"] 
        (NoArg (\o -> o { optKey = True }))
        "Output JSON as an array of objects keyed by a given column, KEY, rather than as a list"
    , Option "i"  ["indent"]
        (NoArg (\o -> o { optIndent = True }))
        "Indent the output JSON this many spaces. Disabled by default."
     , Option "s"  ["stream"]
        (NoArg (\o -> o { optStream = True }))
        "Output JSON as a stream of newline-separated objects, rather than an as an array."
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help message and exit."]

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (L.foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError
                            (L.concat errors ++ usageInfo usage options))

parseData :: String -> IO [[String]]
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result

processFileStream :: Options -> [String] -> IO()
processFileStream opts args = do
    fileData <- if  (L.last args) == "-" then getContents   else readFile (L.last args)
    table <- parseData fileData
    mapM_ putStrLn $ L.map (\ x -> streamOfObjects (L.head table) x) (L.tail table)

processFileKey :: Options -> [String] -> IO()
processFileKey opts args = do
    fileData <- if  (L.last args) == "-" then getContents   else readFile (L.last args)
    table <- parseData fileData
    let key = L.head args
    let s = L.concat $ L.map (\ x -> (L.concat $ L.intersperse "," x) ++ "\n") table
    let xs = fromCsvNamed (T.pack s)
    let ys = [(findValue (xs !! i) key) | i <- [0..(L.length (L.head table))]]
    let zs = L.map (\ x -> "  " ++ (fst x) ++ "  " ++ (snd x)) (L.zip ys (L.map show xs)) 
    if (L.head args) `elem` (L.head table) then (mapM_ putStr zs) else (print (L.head args))

processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  (L.last args) == "-" then getContents   else readFile (L.last args)
    table <- parseData fileData
    let n = read (L.head args) :: Int
    let s = L.concat $ L.map (\ x -> (L.concat $ L.intersperse "," x) ++ "\n") table 
    let xs = fromCsvNamed (T.pack s)
    putStr "[ \n"
    mapM_ putStr $ L.map (\ x -> (convert (show x) n (L.length (L.head table))) ++ ",\n") xs
    putStr "]"

decide opts arguments | optKey opts    = processFileKey opts arguments
                      | otherwise      = processFile opts arguments

showInfo :: Options -> IO ()
showInfo Options { optVersion = True } = putStrLn $ "csvjson version " ++ show version
showInfo Options { optHelp = True }    = putStr $ usageInfo usage options
showInfo _                             = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
       then showInfo opts
    else (if (L.length arguments > 1) then (decide opts arguments) else (if (optStream opts) then (processFileStream opts arguments) else putStrLn ("Error: Arguments not given")))
