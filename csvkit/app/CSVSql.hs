{-# LANGUAGE OverloadedStrings #-}
import System.Environment
import System.IO
import System.Console.GetOpt
import CSV
import CSV2SQL
import Text.Parsec.Error (ParseError)
import Control.Monad
import Data.List
import Database.HDBC
import Database.HDBC.Sqlite3
import Database.MySQL.Simple

import Blaze.ByteString.Builder (toByteString)
import Data.ByteString (ByteString)
import Data.String (IsString(..))
import qualified Blaze.ByteString.Builder.Char.Utf8 as Utf8
import qualified Data.ByteString as B


--instance IsString Query where
--    fromString = Query . toByteString . Utf8.fromString

version :: Double
version = 0.1

data Options = Options 
    { optHelp           :: Bool
    , optVersion        :: Bool
    , optCSVToFile      :: Bool
    , optMysqlUser      :: Bool
    , optMysqlPassword  :: Bool
    , optDatabaseName   :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp           = False
    , optVersion        = False
    , optCSVToFile      = False
    , optMysqlUser      = False
    , optMysqlPassword  = False
    , optDatabaseName   = False
    }

usage :: String
usage = "Usage: csv_sql inputCSVFile OutputFileName TableName field1,field2,... "

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version" 
    , Option "c"  ["csv2sql"] 
                (NoArg (\o -> o { optCSVToFile = True }))
                "Converts a given csv file to a SQLite Table"
    ,Option "u"  ["username"]
                (NoArg (\o -> o { optMysqlUser = True }))
                "user name for mysql"
    ,Option "p"  ["password"]
                (NoArg (\o -> o { optMysqlPassword = True }))
                "password for mysql"
     ,Option "d"  ["database"]
                (NoArg (\o -> o { optDatabaseName = True }))
                "database name"
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help"]

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))
              
parseData :: String -> [[String]]
parseData dat = case parseCSV dat of 
    Right result -> result

convertCSVFileToSQL :: [String] -> IO ()
convertCSVFileToSQL args = do
--convertCSVFileToSQL inFileName outFileName tableName fields = do
  input <- if head args == "-" then getContents    else readFile (head args)
  let records = parseData input
  convertCSVToSQL tableName outFileName fields records 
    where tableName = concat $ take 1 (drop 2 args)
          outFileName = concat $ take 1 (drop 1 args)
          fields = drop 3 args

showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "csvsql version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"


mysqlInsertQuery :: [String] -> IO ()
mysqlInsertQuery inputs = do
    conn <- connect defaultConnectInfo
        { connectUser = inputs !! 0
        , connectPassword = inputs !! 1
        , connectDatabase = inputs !! 2
        }

    let tableName = inputs !! 3
    let s = inputs !! 4
    
    
--    Database.MySQL.Simple.execute conn (fromString "insert into " ++ tableName ++ " (" ++ s ++ ") values (?, ?, ?)") ["1" :: String, "Edwin" :: String, "Brady" :: String]
    
    close conn


main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts
        then showInfo opts
    else if optCSVToFile opts
        then convertCSVFileToSQL arguments
    else if optMysqlUser opts && optMysqlPassword opts && optDatabaseName opts
        then mysqlInsertQuery arguments
    else putStrLn "Give Valid Options."
