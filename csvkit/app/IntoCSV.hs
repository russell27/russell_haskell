import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List (intercalate)
import Data.Xls (decodeXlsIO)

version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optxls2csv     :: Bool
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optxls2csv   = True
    }

--lookOptionalArgs :: [Look]
--lookOptionalArgs = 
  --          [Table.unicodeTable
    --        , Table.unicodeWithoutHeader
      --      , Table.asciiTable
        --    , Table.asciiWithoutHeader
          --  , Table.unicodeWithNumbering
            --]

usage :: String
usage = "Usage: stack exec -- xls_csv [OPTION...] fileName"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "x"  ["xls-csv"]
        (NoArg (\o -> o { optxls2csv = True }))
        "Convert the xls format file to csv"
    , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show help"] 
        -- ++ moreOptions
        -- where
        --moreOptions = map converterToOption lookOptionalArgs
        --converterToOption c@(Look name _ short long) = 
         --   Option short [long] 
          --      (NoArg (\o -> o {optLookTool = c}))
            --    ("Prints " ++ name)


getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError 
                            (concat errors ++ usageInfo usage options))

showInfo :: Options -> IO ()
showInfo Options { optVersion = True }      = putStrLn $ "xls_csv version " ++ show version
showInfo Options { optHelp = True }         = putStr $ usageInfo usage options
showInfo _                                  = fail "unable to show info for weird options"

xlsToCSV :: String -> IO ()
xlsToCSV file = do
    worksheets <- decodeXlsIO file
    mapM_ (mapM_ (putStrLn . intercalate ",")) worksheets
    
--main :: Options
main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
        then showInfo opts
    else xlsToCSV (head arguments)

