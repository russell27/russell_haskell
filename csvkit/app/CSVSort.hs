{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
import Sort
import SortOptions
import System.Environment
import System.IO
import System.Console.GetOpt
import Data.List
import Data.Char
import Data.Function
import CSV

version :: Double
version = 0.1

data Options = Options 
    { optHelp        :: Bool
    , optVersion     :: Bool
    , optGetHeader   :: Bool
    , optSortOrd     :: Sort
    }

defaultOptions :: Options
defaultOptions = Options
    { optHelp      = False
    , optVersion   = False
    , optGetHeader = False
    , optSortOrd   = SortOptions.ascending
    }

sortOptionalArgs :: [Sort]
sortOptionalArgs = [SortOptions.ascending, SortOptions.descending]

usage :: String
usage = "Usage: stack exec -- csvsort [OPTION...] fileName colNames / colIndices"

options :: [OptDescr (Options -> Options)]
options = 
    [ Option "v"  ["version"] 
        (NoArg (\o -> o { optVersion = True }))
        "show version"
    , Option "n"  ["names"]
        (NoArg (\o -> o { optGetHeader = True }))
        "Display column names and indices from the input CSV and exit."
     , Option "h"  ["help"] 
        (NoArg (\o -> o { optHelp = True }))
        "show this help message and exit."] ++ moreOptions
        where
        moreOptions = map converterToOption sortOptionalArgs
        converterToOption c@(Sort name _ short long) = 
            Option short [long] 
                (NoArg (\o -> o {optSortOrd = c}))
                ("Sort in " ++ name ++ " order.")
 

getOptions :: [String] -> IO (Options, [String])
getOptions argv =
    case getOpt Permute options argv of
        (o, n, []) -> return (foldl (flip id) defaultOptions o, n)
        (_, _, errors) -> ioError (userError
                            (concat errors ++ usageInfo usage options))

processOpts :: Table -> Int -> Options -> Table
processOpts table n (Options _ _ _ (Sort { sSort = f })) = f table n

parseData :: String -> IO [[String]]
parseData dat = case parseCSV dat of 
    Left _ -> ioError $ userError "unable to parse file"
    Right result -> return result

processFileH :: Options -> [String] -> IO ()
processFileH opts args = do
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    putStrLn $ unlines $  map (\ x -> (fst x) ++ ":" ++ (snd x)) (zip (map show [1..]) (head table))

processFile :: Options -> [String] -> IO ()
processFile opts args = do  
    fileData <- if  (head args) == "-" then getContents    else readFile (head args)
    table <- parseData fileData
    let colIndex = getIndex (head $ words (last args)) (head table)
    mapM_ putStrLn $ map convert (processOpts table colIndex opts)

showInfo :: Options -> IO ()
showInfo Options { optVersion = True } = putStrLn $ "csvsort version " ++ show version
showInfo Options { optHelp = True }    = putStr $ usageInfo usage options
showInfo _                             = fail "unable to show info for weird options"

main = do
    args <- getArgs
    (opts, arguments) <- getOptions args
    hSetEncoding stdout utf8
    if optHelp opts || optVersion opts 
       then showInfo opts
    else (if (optGetHeader opts) then (processFileH opts arguments) 
    else (if (length arguments > 1) then (processFile opts arguments) else putStrLn ("Error: Arguments not given")))
