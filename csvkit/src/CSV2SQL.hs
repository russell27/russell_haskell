module CSV2SQL(convertCSVToSQL, determineDataTypes, zipWithSpace) where
import Text.CSV
import Database.HDBC
import Database.HDBC.Sqlite3
import Data.List
import Data.Char
import Data.Function

-- Returns "Successful" if successful,
-- error message otherwise.
convertCSVToSQL :: String -> FilePath -> [String] -> CSV -> IO ()
convertCSVToSQL tableName databaseName fields records =
 -- Check to make sure that the number of
 -- columns matches the number of fields
 if nfieldsInFile == nfieldsInFields then do
 -- Open a connection
   conn <- connectSqlite3 databaseName
   -- Create a new table
   run conn createStatement []
   -- Load contents of CSV file into table
   stmt <- prepare conn insertStatement
   executeMany stmt (tail (filter (\record -> nfieldsInFile == length record) sqlRecords))
   -- Commit changes
   commit conn
   -- Close the connection
   disconnect conn
   -- Report that we were successful
   putStrLn "Successful"
 else
   putStrLn "The number of input fields differ from the csv file."
                         where
                           fieldsNvalues = zipWithSpace fields (tellDataType (tail records))
                           nfieldsInFile = length $ head records
                           nfieldsInFields = length fields

                           createStatement = "CREATE TABLE " ++ tableName ++ " (" ++ (intercalate "," fieldsNvalues) ++ " )"
                           insertStatement =  "INSERT INTO " ++ tableName ++ " VALUES (" ++ intercalate ","(replicate nfieldsInFile "?") ++ ")"
                           sqlRecords = map (\record -> map (\element -> toSql element) record) records



-- generate string for field-names and their corresponding data-types 
zipWithSpace :: [String] -> [String] -> [String]
zipWithSpace fieldNames dataTypes = [f ++ " " ++ d | (f,d) <- zip fieldNames dataTypes]

-- tells the most common datatype for each column 
tellDataType :: CSV -> [String]
tellDataType records = [mostCommon x | x <- (transpose (listDataTypes (records)))]

mostCommon :: Ord a => [a] -> a
mostCommon = head . maximumBy (compare `on` length) . group . sort

-- lists the data-type of each element in a given column
listDataTypes :: CSV -> CSV
listDataTypes records = [[determineDataTypes value |value <- row] |  row <- records]

-- parses each record of a CSV file and determines the data-type of each element in the record
determineDataTypes :: String -> String
determineDataTypes value
--        | value == "null" || value == null = "NULL"
        | (all (==True) (map isDigit value)) = "INTEGER"
        | count '.' value == 1 && isReal value = "REAL"
        | otherwise = "TEXT"

count :: Eq a => a -> [a] -> Int
count x = length . filter (x==)

isReal :: String -> Bool
isReal xs = (all (==True) (map isDigit $ filter (not . (== '.')) xs))

