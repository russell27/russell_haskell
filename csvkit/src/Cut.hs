module Cut where

type Table = [[String]]

data Cut = Cut
    { cName         :: String
    , cCut         :: Table -> String -> Table
    , cShortOpt     :: String
    , cLongOpt      :: String
    }