module CutOptional (columns, not_columns) where
import Cut
import Prelude
import System.Environment
import System.Directory
import Data.Char
import System.IO
import Data.List
import Text.CSV
import Data.Char (isDigit)

isNum :: String -> [Bool]
isNum xs = nub $ map isDigit (filter (/=' ') xs)

isValid :: String -> Bool
isValid xs = (length (isNum xs)) == 1

getNotIndices :: Table -> [Int] -> [Int]
getNotIndices xs cols = [1..(length (head xs))] \\ cols


getCols :: Table -> String -> Table
getCols xs cols | (isValid cols) && head (isNum cols) = map (\ n -> transpose(xs) !! (n-1)) $ map (\ x -> (read x :: Int)) $ words cols
                | otherwise                           = map (\ n -> transpose(xs) !! (n-1)) $ map (\ name -> (length $ takeWhile (/= name) (head xs)) + 1) (words cols)

getNotCols :: Table -> String -> Table
getNotCols xs cols | (isValid cols) && head (isNum cols) = map (\ n -> transpose(xs) !! (n-1)) $ getNotIndices xs (map (\ x -> (read x :: Int)) $ words cols)
                | otherwise                           = map (\ n -> transpose(xs) !! (n-1)) $ getNotIndices xs (map (\ name -> (length $ takeWhile (/= name) (head xs)) + 1) (words cols))


columns :: Cut
columns = Cut
    { cName        = "Gives columns of given indices. Indices are to be provides as a space separated list of column indices with quotes, e.g. '1 2 3'. Defaults to all columns."
    , cCut         = getCols
    , cShortOpt    = "c"
    , cLongOpt     = "columns"
    }

not_columns :: Cut
not_columns = Cut
    { cName        = "Gives columns of indices not given. Indices are to be provides as a space separated list of column indices with quotes, e.g. '1 2 3'. Defaults to all columns."
    , cCut         = getNotCols
    , cShortOpt    = "C"
    , cLongOpt     = "not-columns"
    }
    
