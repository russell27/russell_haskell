module CleanOptions (getErrors, format) where

format :: [(Integer, Int)] -> [[String]] -> [String]
format xs table = map (\ x -> "Line " ++ (show (fst x)) ++  " : " ++ "expected " ++ (show (length $ head table)) ++ " columns, but found " ++ (show (snd x)) ++ " columns") xs

getErrors :: [[String]] -> [(Integer, Int)]
getErrors xs = [(x, y) | (x, y) <- (zip [2..] (map length (tail xs))), y /= (length $ head xs)]
