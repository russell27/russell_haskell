module JSONOptions (indent, convert, findValue, streamOfObjects) where

import Data.List
import Dynamic

indent :: String -> Int -> Int -> String
indent s n x = concat $ [ if i == 0 then ((ys !! i) ++ " \n") else ((replicate (2 * n) ' ') ++ (ys !! i) ++ " \n") | i <- (filter even [0..x])]
                             where ys = map (\ x -> (fst x) ++ (snd x)) (zip (words s) $ tail (words s))

convert :: String -> Int -> Int -> String
convert s n x = (replicate n ' ') ++ [(head s)] ++ (replicate n ' ') ++ (indent (tail (init s)) n x) ++ (replicate n ' ') ++ [(last s)]

findValue :: Dynamic -> String -> String
findValue xs key = head [(snd i) | i <- ys, (fst i) == (show key)]
                         where ys = zip (map show (toKeys xs)) (map show (toElems xs))

streamOfObjects :: [String] -> [String] -> String
streamOfObjects xs ys = "{" ++ (concat $ map (\ x -> (show (fst x)) ++ " : " ++ (snd x) ++ ", ") (zip xs (init ys))) ++ (last xs) ++ " : "  ++ (last ys) ++ "}"

