module Stack(countLinesInFile) where

type Table = [[String]]

data Stack = Stack
    { cName         :: String
    , cStack        :: [Table] -> Table
    , cShortOpt     :: String
    , cLongOpt      :: String
    }
countLinesInFile :: String -> IO Int
countLinesInFile filename = do
  content <- readFile filename
  return $ length ( lines content ) - 1