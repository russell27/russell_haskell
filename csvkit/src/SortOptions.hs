module SortOptions(ascending, descending, convert, getIndex) where
import Sort
import Prelude
import System.Environment
import System.Directory
import Data.Char
import Data.Function
import System.IO
import Data.List
import Data.Maybe
import Text.CSV
import Data.Char (isDigit)

getIndex :: String -> [String] -> Int
getIndex s xs | and $ map isDigit s = (read s :: Int) - 1
              | otherwise           = fromJust $ elemIndex s xs

convert :: [String] -> String
convert = intercalate ","

removeNullRows :: Table -> Table
removeNullRows table = [xs | xs <- table, length (head table) == length xs]

formTuple :: [String] -> Int -> (String, [String])
formTuple xs n = ((xs !! n), (delete (xs !! n) xs))

getRows :: Table -> Int -> [(String, [String])]
getRows xs colIndex = map (\ x -> formTuple x colIndex) xs

sortBynthCol :: Table -> Int -> [(String, [String])]
sortBynthCol xs n = sortBy compare (getRows (tail xs) n)

insertAt :: a -> Int -> [a] -> [a]
insertAt q 0 (_:xs) = q : xs
insertAt q n (x:xs) = x : insertAt q (n-1) xs

arrangeTable :: Table -> Int -> Table
arrangeTable xs n | n /= 0    = map (\ x -> insertAt (fst x) n (snd x)) (sortBynthCol xs n)
                  | otherwise = map (\ x -> [fst x] ++ snd x) (sortBynthCol xs n) 

ascendOrd :: Table -> Int -> Table
ascendOrd xs n = [head xs] ++ arrangeTable (removeNullRows xs) n

descendOrd ::  Table -> Int -> Table
descendOrd xs n = [head xs] ++ reverse (arrangeTable (removeNullRows xs) n)

ascending :: Sort
ascending = Sort
    { sName        = "ascending"
    , sSort        = ascendOrd
    , sShortOpt    = "I"
    , sLongOpt     = "no-inference"
    }

descending :: Sort
descending = Sort
    { sName        = "descending"
    , sSort        = descendOrd
    , sShortOpt    = "r"
    , sLongOpt     = "descending"
    } 
