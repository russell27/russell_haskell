module Look where

type Table = [[String]]

data Look = Look
    { cName         :: String
    , cLook         :: Table -> String
    , cShortOpt     :: String
    , cLongOpt      :: String
    }

