module FormatOptions (setDelimiter, setTerminator, quoteChar, getElems) where
import Prelude
import System.Environment
import System.Directory
import Data.Char
import Data.Function
import System.IO
import Data.List
import Text.CSV

setDelimiter :: String -> [[String]] -> [String]
setDelimiter s table = map (\ x -> intercalate s x) table

setTerminator :: String -> [[String]] -> [String]
setTerminator s table = setDelimiter "," (map (++ [s]) table)

charToString :: Char -> String
charToString c = [c]

changeChar :: Char -> String -> String
changeChar c s | (charToString c) == s = charToString c ++ s
               | otherwise             = charToString c

changeStr :: String -> String -> String
changeStr s c | (head c) `elem` s = c ++ concat (map (\ x -> changeChar x c) s) ++ c
              | otherwise         = s  

quoteChar :: String -> [[String]] -> [[String]]
quoteChar c table = map (\ xs -> (map (\ s -> changeStr s c) xs)) table

getElems :: Char -> [String] -> [String]
getElems s xs = map (\ x -> if s `elem` x then (show x) else x) xs
