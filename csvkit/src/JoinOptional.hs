module JoinOptional (inner, left, right, zipWithPadding) where
import Join
import Prelude
import System.Environment
import System.Directory
import Data.Char
import System.IO
import Data.List as L
import Text.CSV
import Data.Char (isDigit)

deleteElem :: Int -> [a] -> [a]
deleteElem _ [] = []
deleteElem x zs | x > 0 = L.take (x-1) zs ++ L.drop x zs
                | otherwise = zs

removingEmpty :: Table -> Table
removingEmpty xs = [x | x <- xs, x/=[] ] 

removeMultiple :: Int -> Int -> Table -> Table
removeMultiple a b xs = L.map (deleteElem (a+b-1)) xs

zipWithPadding :: String -> String -> Table -> Table -> [([String],[String])]
zipWithPadding a b (x:xs) (y:ys) = (x,y) : zipWithPadding a b xs ys
zipWithPadding a _ []     ys     = zip (repeat [a]) ys
zipWithPadding _ b xs     []     = zip xs (repeat [b])

joining :: Int -> Table -> Table -> Table
joining i a b = [if ((fst x)!!(i-1)) == ((snd x)!!(i-1)) then (snd x) else (fst x)| x <- zip_list ]
 where zip_list = zipWithPadding "" "" a b

concatLeft :: Table -> Table -> Int -> Table
concatLeft xs ys cols = [if ((xs !! i) !! (cols-1)) == ((ys !! j) !! (cols-1))  then ((xs !! i) ++ (ys !! j)) else [] | i <- [0..((length xs) - 1)], j <- [0..((length ys) - 1)]]
                      
concatRight :: Table -> Table -> Int -> Table
concatRight xs ys cols = [if ((ys !! i) !! (cols-1)) == ((xs !! j) !! (cols-1))  then ((ys !! i) ++ (xs !! j)) else [] | i <- [0..((length ys) - 1)], j <- [0..((length xs) - 1)]]

innerc :: Table -> Table -> Int -> Table
innerc a b i = [((fst x) ++ (snd x))| x <- zip_list, (fst x)!!(i-1) == (snd x)!!(i-1)]
    where zip_list = zipWithPadding "" "" a b
                  

leftJoin :: Table -> Table -> String -> Table
leftJoin a b cols = joining (read cols :: Int) a (removeMultiple (length a) (read cols :: Int) (removingEmpty $ (concatLeft a b (read cols :: Int))))

rightJoin :: Table -> Table -> String -> Table
rightJoin a b cols = joining (read cols :: Int) b (removeMultiple (length b) (read cols :: Int) (removingEmpty $ (concatRight a b (read cols :: Int))))

innerJoin :: Table -> Table -> String -> Table
innerJoin a b cols = removeMultiple (length a) (read cols :: Int) (innerc a b (read cols :: Int)) 

inner :: Join
inner = Join
    { jName        = "Performs SQL InnerJoin bases on the given index refernce on given CSV files. A single index is to be provided with quotes, e.g. '1'. Defaults to all columns."
    , jJoin        = innerJoin
    , jShortOpt    = "i"
    , jLongOpt     = "columns"
    }
    
left :: Join
left = Join
    { jName        = "Performs SQL LeftJoin bases on the given index refernce on given CSV files. A single index is to be provided with quotes, e.g. '1'. Defaults to all columns."
    , jJoin        = leftJoin
    , jShortOpt    = "l"
    , jLongOpt     = "left"
    }
    
right :: Join
right = Join
    { jName        = "Performs SQL RightJoin bases on the given index refernce on given CSV files. A single index is to be provided with quotes, e.g. '1'. Defaults to all columns."
    , jJoin        = rightJoin
    , jShortOpt    = "r"
    , jLongOpt     = "right"
    }


