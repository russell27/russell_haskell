module StatOptions (mins, maxs, sums, mean, median, stdev, toInts, medians, means, rowCount, getCols, formatFreqs, rmElems, setNumsNames, freqElems, isNumericCol, getColumns, applyFunction, howManyUnique, isNullCol, longestLen, numstdev, texts, allCols, getFreqs, formatResult, freqAllCols, freqGivenCols) where
import Stat
import Prelude
import System.Environment
import System.Directory
import Data.Char
import Data.Function
import System.IO
import Data.List
import Text.CSV
import Data.Maybe
import Data.Char (isDigit)

isNum :: String -> [Bool]
isNum s = nub $ map isDigit (filter (/=' ') s)

isValid :: String -> Bool
isValid s = (length (isNum s)) == 1

getCols :: Table -> String -> Table
getCols xs cols | (isValid cols) && head (isNum cols) = map (\ n -> [(show n)] ++ transpose(xs) !! (n-1)) $ sort $ map (\ x -> (read x :: Int)) $ words cols
                | otherwise                           = map (\ n -> [(show n)] ++ transpose(xs) !! (n-1)) $ sort $ map (\ name -> (length $ takeWhile (/= name) (head xs)) + 1) (words cols)

setNumsNames xs = map (\ x -> [(head x) ++ ". " ++ (x !! 1)] ++ x) xs

rmElems xs = map (\ x -> delete (x !! 1) x) xs

toInts :: [String] -> [Int]
toInts xs = map (\ x -> (read x :: Int)) (tail (filter (not.null) xs))

minOfCols :: Table -> [Int]
minOfCols xs = map (\ y -> minimum (toInts y)) ys
                        where ys = (map (filter (not.null)) xs)

maxOfCols :: Table -> [Int]
maxOfCols xs = map (\ x -> maximum (toInts x)) ys
                               where ys = map (filter (not.null)) xs

sumOfCols :: Table -> [Int]
sumOfCols xs = map (\ x -> sum (toInts x)) ys
                               where ys = map (filter (not.null)) xs

mean :: [Int] -> Int
mean xs = div (sum xs) (length xs)

meanOfCols :: Table -> [Int]
meanOfCols xs = map (\ x -> mean (toInts x)) ys
                               where ys = map (filter (not.null)) xs

median :: [String] -> Int
median xs | odd (length xs)  = ys !! mid
          | otherwise        = (ys !! mid + ys !! (mid + 1)) `div` 2
                  where mid = (length ys) `div` 2
                        ys = sort $ toInts xs

medianOfCols :: Table -> [Int]
medianOfCols xs = map (\ x -> (median x)) ys
                                 where ys = map (filter (not.null)) xs

toFloats :: [String] -> [Float]
toFloats xs = map (\ x -> read x :: Float) (tail xs)

stdev :: [String] -> Float
stdev xs = sqrt . average . map ((^2) . (-) axs) $ ys
           where ys = toFloats xs
                 average = (/) <$> sum <*> realToFrac . length
                 axs = average ys

freqElems :: [String] -> [(String, Int)]
freqElems xs = sortBy (compare `on` snd) $ map (\ n -> (n, (length $ filter (== n) xs))) (nub (filter (not.null) (tail xs)))

isNullCol :: [String] -> Bool
isNullCol xs = or $ map null (tail xs)

isNumericCol :: [String] -> String
isNumericCol xs | and $ concat $ map isNum (tail xs) = "Number"
                | otherwise                          = "Text"

howManyUnique :: [String] -> Int
howManyUnique xs = length $ nub (tail xs)

longestLen :: [String] -> Int
longestLen xs = maximum $ map length (tail xs)

rowCount :: Table -> String
rowCount table = show (length $ tail $ table) ++ " rows"

applyFunction f table cols = map (\ y -> (fst y) ++ " : " ++ show (snd y)) (zip (map (\ x -> head x) ys) (map f ys))
                                 where ys = (rmElems $ rmElems $ setNumsNames $ getCols table cols)

getColumns :: Table -> String -> [String]
getColumns table cols = map (\ x -> (intercalate "," x)) (getCols table cols)

numstdev :: Table -> String -> [String]
numstdev table cols = map (\ x -> (fst x) ++ ": " ++ show (snd x) ) 
                          (zip (map head tableNum) (map stdev tableNum))
                          where tableNum = [filter (not.null) x | x <- (rmElems $ rmElems $ setNumsNames $ getCols table cols), isNumericCol x == "Number"]

texts :: Table -> String -> [String]
texts table cols = map (\ x -> (fst x) ++ ": " ++ (snd x)) 
                       (zip tableText (take (length tableText) (repeat "None")))
                       where tableText = [head x | x <- (rmElems $ rmElems $ setNumsNames $ getCols table cols), isNumericCol x == "Text"]

allCols :: Table -> String
allCols table = intercalate " " (map (\ x -> (show x)) [1..n]) where n = length $ head $ table

getFreqs :: Table -> String -> Int -> [[(String, Int)]]
getFreqs table s n = map (\ y -> (take n (reverse $ tail y))) ys
                        where ys = map (\ x -> freqElems x) (getCols table s)

formatResult :: [[(String, Int)]] -> [String]
formatResult xs = map (\ y -> "{" ++ (init $ init (concat $ map (\x -> (fst x) ++ ": " ++ show (snd x) ++ ", " ) y)) ++ "}") xs

formatFreqs :: [(String, Int)] -> [String]
formatFreqs ys = map (\ x -> (fst x) ++ " " ++ "(" ++ (show (snd x)) ++ "x)") ys

freqAllCols :: [String] -> Table -> [String]
freqAllCols xs table = map (\ r -> (fst r) ++ " : " ++ (snd r)) (zip (map (\ x -> (fst x) ++ "." ++ (snd x)) (zip (map show [1..]) (head table))) xs)

freqGivenCols :: [String] -> String -> Table -> [String]
freqGivenCols xs s table = map (\ r -> (fst r) ++ " : " ++ (snd r)) (zip (map (\ x -> head x) (rmElems $ rmElems $ setNumsNames $ getCols table s)) xs)

mins :: Stat
mins = Stat
    { sName        = "min"
    , sStat        = minOfCols
    , sShortOpt    = "m"
    , sLongOpt     = "min"
    }

maxs :: Stat
maxs = Stat
    { sName        = "max"
    , sStat        = maxOfCols
    , sShortOpt    = "x"
    , sLongOpt     = "max"
    }

sums :: Stat
sums = Stat
    { sName        = "sum"
    , sStat        = sumOfCols
    , sShortOpt    = "s"
    , sLongOpt     = "sum"
    }

means :: Stat
means = Stat
    { sName        = "mean"
    , sStat        = meanOfCols
    , sShortOpt    = "e"
    , sLongOpt     = "mean"
    }


medians :: Stat
medians = Stat
    { sName        = "median"
    , sStat        = medianOfCols
    , sShortOpt    = "d"
    , sLongOpt     = "median"
    }
