module Text (match, reverseMatch) where
import Grep

findRecords :: Table -> String -> Table
findRecords table text = [head table] ++ [x | x <- (tail table), text `elem` x]

findRecords' :: Table -> String -> Table
findRecords' table text = [head table] ++ [x | x <- (tail table), text `notElem` x]
            
match :: Grep
match = Grep
    { cName        = "records containing a given pattern"
    , cGrep        = findRecords
    , cShortOpt    = "m"
    , cLongOpt     = "match"
    }

reverseMatch :: Grep
reverseMatch = Grep
    { cName        = "records that do not contain a given pattern"
    , cGrep        = findRecords'
    , cShortOpt    = "i"
    , cLongOpt     = "inverse-match"
    }
