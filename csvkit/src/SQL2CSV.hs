module SQL2CSV(toCSV,mySql2CSV) where
import Text.CSV
import Database.HDBC
import Database.HDBC.Sqlite3
import Control.Monad
import Database.MySQL.Simple
import Data.List
import Data.Char
import Data.Function
import Data.Convertible

-- Returns "Successful" if successful,
-- error message otherwise.
toCSV :: String -> String -> IO()
toCSV databaseString query = do
    conn <- connectSqlite3 databaseString
   
    sqlRows <- quickQuery conn query []
    let rowStrings = [[(fromSql a)::String | a <- b] | b <- sqlRows]

    putStr $  unlines [intercalate "," y | y <- rowStrings]

    disconnect conn

mySql2CSV ::String->String->String->Query-> IO ()
mySql2CSV username password db q = do
    conn <- connect defaultConnectInfo
        { connectUser = username
        , connectPassword = password
        , connectDatabase = db
        }

    sqlRows <- query_ conn q 
    print (sqlRows :: [Only String])

    close conn
