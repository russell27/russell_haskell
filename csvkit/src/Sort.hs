module Sort where

type Table = [[String]]

data Sort = Sort
    { sName         :: String
    , sSort         :: Table -> Int -> Table
    , sShortOpt     :: String
    , sLongOpt      :: String
    }
