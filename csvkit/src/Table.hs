module Table (unicodeTable, unicodeWithoutHeader, asciiTable, asciiWithoutHeader,unicodeWithNumbering, unicodeMaxRows, unicodeMaxColumns ) where
import Look

import Data.Maybe
import Text.Layout.Table
import Data.List
import Data.Tuple
import System.Environment
import Text.CSV
import Text.Parsec.Error
import Data.Either.Unwrap

repli :: [a] -> Int -> [a]
repli xs n = concatMap (replicate n) xs

removeEmptyList :: CSV -> CSV
removeEmptyList = filter (\e -> e/=[""]) 

calcSizes :: Foldable t => [t a] -> (Int, Int)
calcSizes file = (length (head file) , length file - 1)

unicodeWithHeaders :: Table -> String
unicodeWithHeaders csvfile = tableString (repli [numCol] (fst sizeTuple))
                           unicodeRoundS
                           (titlesH (head file))
                           [ rowG (file !! x) | x <- [1..(snd sizeTuple)]]
                           where file = removeEmptyList csvfile
                                 sizeTuple = calcSizes file

                            
unicodeWithNumberColumn :: Table -> String
unicodeWithNumberColumn csvfile = tableString (repli [numCol] (fst sizeTuple + 1) )
                                  unicodeRoundS
                                  (titlesH ("Number" : head file))                       
                                  [ rowG (show x : (file !! x)) | x <- [1..(snd sizeTuple)]]
                                  where file = removeEmptyList csvfile
                                        sizeTuple = calcSizes file

unicodeWithoutHeaders :: Table -> String
unicodeWithoutHeaders csvfile = tableString (repli [numCol] $ fst sizeTuple)
                            unicodeRoundS
                            def
                            [ rowG (file !! x) | x <- [0..(snd sizeTuple)]]
                            where file = removeEmptyList csvfile
                                  sizeTuple = calcSizes file

asciiWithHeaders :: Table -> String
asciiWithHeaders csvfile = tableString (repli [numCol] $ fst sizeTuple)
                           asciiS
                           (titlesH (head file))
                           [ rowG (file !! x) | x <- [1..(snd sizeTuple)]]
                           where file = removeEmptyList csvfile
                                 sizeTuple = calcSizes file


asciiWithoutHeaders :: Table -> String
asciiWithoutHeaders csvfile = tableString (repli [numCol] $ fst sizeTuple)
                              asciiS
                              def
                              [ rowG (file !! x) | x <- [0..(snd sizeTuple)]]
                              where file = removeEmptyList csvfile
                                    sizeTuple = calcSizes file

maxRows :: Table -> Int -> Table
maxRows table rowsCount = if size > rowsCount
        then take rowsCount table ++ [extraRows]
        else take rowsCount table
        where size = length table
              extraRows = replicate (length (head table)) ".."

unicodeMaxRows :: Table -> Int -> String
unicodeMaxRows csvfile rowsCount = tableString (repli [numCol] $ fst sizeTuple)
                            unicodeRoundS
                            def
                            [ rowG (file !! x) | x <- [0..(snd sizeTuple)]]
                            where file = maxRows (removeEmptyList csvfile) rowsCount
                                  sizeTuple = calcSizes file
maxColumns :: Table -> Int -> Table
maxColumns table colCount = if size > colCount 
        then transpose $ take colCount table' ++ [extraCols]
        else transpose $ take colCount table'
          where table' = transpose table
                size = length table
                extraCols = replicate size ".."

unicodeMaxColumns :: Table -> Int -> String
unicodeMaxColumns csvfile rowsCount = tableString (repli [numCol] $ fst sizeTuple)
                            unicodeRoundS
                            def
                            [ rowG (file !! x) | x <- [0..(snd sizeTuple)]]
                            where file = maxColumns (removeEmptyList csvfile) rowsCount
                                  sizeTuple = calcSizes file

unicodeTable :: Look
unicodeTable =  Look
    { cName        = "unicode table"
    , cLook        = unicodeWithHeaders
    , cShortOpt    = "u"
    , cLongOpt     = "unicode-table"
    }

unicodeWithoutHeader :: Look
unicodeWithoutHeader = Look
    { cName        = "unicode table that doesn't include headers"
    , cLook        = unicodeWithoutHeaders
    , cShortOpt    = "l"
    , cLongOpt     = "headerless-unicode"
    }

asciiTable :: Look
asciiTable =  Look
    { cName        = "ascii table"
    , cLook        = asciiWithHeaders
    , cShortOpt    = "a"
    , cLongOpt     = "ascii-table"
    }

asciiWithoutHeader :: Look
asciiWithoutHeader = Look
    { cName        = "ascii table that doesn't include headers"
    , cLook        =  asciiWithoutHeaders
    , cShortOpt    = "n"
    , cLongOpt     = "headerless-ascii"
    }
unicodeWithNumbering :: Look
unicodeWithNumbering = Look
    { cName        = "unicode table along with line numbers"
    , cLook        = unicodeWithNumberColumn
    , cShortOpt    = "e"
    , cLongOpt     = "line-numbering"
    }
