module Join where

type Table = [[String]]

data Join = Join
    { jName         :: String
    , jJoin         :: Table -> Table -> String -> Table
    , jShortOpt     :: String
    , jLongOpt      :: String
    }
