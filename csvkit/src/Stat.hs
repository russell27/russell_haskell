module Stat where

type Table = [[String]]

data Stat = Stat
    { sName         :: String
    , sStat         :: Table -> [Int]
    , sShortOpt     :: String
    , sLongOpt      :: String
    }
