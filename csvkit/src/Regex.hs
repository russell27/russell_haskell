module Regex (matchR,matchRegex) where
import Grep
import Text.Regex.Posix
import Data.List

--regexBool r l = l == r :: Bool 

matchRegex :: Table -> String -> Table
matchRegex table pattern = [head table] ++ [ x | x <- table , (intercalate "," x) =~ pattern :: Bool]



matchR :: Grep
matchR = Grep
    { cName        = "records that contain a given regular expresion."
    , cGrep        = matchRegex
    , cShortOpt    = "r"
    , cLongOpt     = "match-regex"
    }

