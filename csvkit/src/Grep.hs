module Grep where

type Table = [[String]]

data Grep = Grep
    { cName         :: String
    , cGrep         :: Table -> String -> Table
    , cShortOpt     :: String
    , cLongOpt      :: String
    }
