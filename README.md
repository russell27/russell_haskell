***Intro***
<br/><br/>
*Substantive software tool-set to do data science on the command line written in Haskell, inspired by csvkit(python)*
<br/>
<br/>
***Contents***

[[_TOC_]]

# **What drives this project?**
We have talked about it in our [Wiki Page](https://gitlab.com/russell27/russell_haskell/-/wikis/home)
<br/><br/>

# **Build Tool Used**
Haskell Tool Stack
  

# **Stack Setup**
Its [Documentation](https://docs.haskellstack.org/en/stable/README/#how-to-install) is good! But, we have also written articles regarding the build tool [Stack](https://medium.com/@nmamatha1808/haskell-tool-stack-setup-c8b1edf0101a) and [IDE](https://medium.com/@pravallikanakarikanti/intellij-idea-setup-for-stack-project-b4b65186d6d3) setup that cover the problems we faced while setting up the environment, which maybe helpful to you.

# **Project Setup**
If you are interested in contributing to or exploring this project then the starting point is - Stack Project Directory i.e. [csvkit](https://gitlab.com/russell27/russell_haskell/-/tree/master/csvkit).<br/>
After cloning the repository, "cd" to the project directory and build it using- 
<br/>
<br/>
    ``` 
     $ stack build
    ```

This command imports all the packages required for the project.

Since it is a commandline tool-set, we have to run it's executable, to work with all the commandline tools that are built so far, use:
<br/>
<br/>
     ```
     $ stack exec -- csvkit-exe --help 
     ```
                  OR
     ```
     $ stack exec -- csvkit-exe -?
     ```
    
"stack exec" command will modify your PATH variable to include a number of additional directories, including the internal executable destination, and your build tools.

# **List Of Tools**
## csv_look 
Default Option - Converts given CSV Data into an unicode table.
 
```
Usage: csv_look [OPTION...] fileName
  -v  --version             show version
  -r  --trunc-rows          Prints truncated table with maximum number of rows as specified
  -c  --trunc-columns       Prints truncated table with maximum number of columns as specified
  -h  --help                show help
  -u  --unicode-table       Prints unicode table
  -l  --headerless-unicode  Prints unicode table that doesn't include headers
  -a  --ascii-table         Prints ascii table
  -n  --headerless-ascii    Prints ascii table that doesn't include headers
  -e  --line-numbering      Prints unicode table along with line numbers
```
## csv_grep
Default Option - Returns records containing a given regex.
 
```
Usage: csv_grep [OPTION...] fileName pattern
  -v  --version        show version
  -g  --get-header     gets the header, Usage: [OPTION] fileName
  -h  --help           this is help
  -m  --match          Returns records containing a given pattern
  -i  --inverse-match  Returns records that do not contain a given pattern
  -r  --match-regex    Returns records that contain a given regular expresion.
  ```
## csv_stat
Gives statistics for a given CSV file.
 
 ```
Usage: csv_stat [OPTION...] fileName columns
  -v  --version     show version
  -n  --names       Display column names and indices from the input CSV and exit.
  -t  --type        Only output data type.
  -r  --count       Only output total row count.
  -c  --columns     A comma separated list of column indices or names to be examined. Defaults to all columns.
  -N  --nulls       Only output whether columns contains nulls.
  -f  --freq        Only output lists of frequent values.
  -q  --freq-count  The maximum number of frequent values to display.
  -u  --unique      Only output counts of unique values.
  -l  --len         Only output the length of the longest values.
  -z  --stdev       Only output standard deviations.
  -h  --help        show this help message and exit
  -m  --min         Only output mins.
  -x  --max         Only output maxs.
  -s  --sum         Only output sums.
  -e  --mean        Only output means.
  -d  --median      Only output medians.
```
## csv_cut
Filters and Truncates a given CSV File.
```
Usage: csv_cut [OPTION...] fileName pattern
  -v  --version            show version
  -n  --names              Display column names and indices from the input CSV and exit.
  -x  --delete-empty-rows  Removes Empty rows in CSV File.
  -h  --help               show this help
  -c  --columns            Does Gives columns of given indices. Indices are to be provides as a space separated list of column indices with quotes, e.g. '1 2 3'. Defaults to all columns.
  -C  --not-columns        Does Gives columns of indices not given. Indices are to be provides as a space separated list of column indices with quotes, e.g. '1 2 3'. Defaults to all columns.
```
## csv_join
Default Option - Execute a SQL-like innerjoin to merge CSV files. 
```
Usage: stack exec -- csv_join [OPTION...] pattern  file1Name  file2Name
  -v  --version            show version
  -h  --help               show this help
  -i  --columns            Performs SQL InnerJoin bases on the given index refernce on given CSV files. A single index is to be provided with quotes, e.g. '1'. Defaults to all columns.
  -l  --left               Performs SQL LeftJoin bases on the given index refernce on given CSV files. A single index is to be provided with quotes, e.g. '1'. Defaults to all columns.
  -r  --right              Performs SQL LeftJoin bases on the given index refernce on given CSV files. A single index is to be provided with quotes, e.g. '1'. Defaults to all columns.
```
## csv_stack
Default Option - Stacks up the given CSV Files. (assumes that the first line of each file is header and header of the first file is only considered.)
```
 Usage: csv_stack [OPTION...] fileName1 fileName2 ..
  -v  --version  show version
  -s  --stack    stacks up given csv files
  -g  --group    group names for each file
  -h  --help     show this help
```
## csv_sort
Default Option - Given a filepath and column index or name it sorts the respective column and returns it.
 
```
Usage: csv_sort [OPTION...] fileName columns
  -v  --version       show version
  -n  --names         Display column names and indices from the input CSV and exit.
  -h  --help          show this help message and exit.
  -I  --no-inference  Sort in ascending order.
  -r  --descending    Sort in descending order.
  ```
## csv_format 
Default Option - Given a CSV File it returns the file with tabs as the delimiter.

 ```
Usage: csv_format [OPTION...] fileName delimiter
  -v  --version             show version
  -T  --out-tabs            Specify that the output CSV file is delimited with tabs. Overrides -D.
  -h  --help                show this help message and exit.
  -D  --delimiter           Delimiting character of the output CSV file.
  -M  --out-terminator      Character used to terminate lines in the output CSV file.
  -Q  --out-quotechar       Character used to quote strings in the output CSV file.
  -B  --out-no-doublequote  Whether or not double quotes are doubled in the output CSV file.
  -U  --out-quoting         Quoting style used in the output CSV file. 0 = Quote Minimal, 1 = Quote All, 2 = Quote Non-numeric, 3 = Quote None.
 ```
 
## csv_clean
Default Option - Does a dry run on the CSV File and returns errors(if any).
 
```
Usage: csv_clean [OPTION...] fileName
  -v  --version  show version
  -n  --dry-run  Do not create output files. Information about what would have been done will be printed to STDERR.
  -h  --help     show this help message and exit.
```
## csv_sql
Default Option - Generates a .db sqlite3 file for a given CSV(identifies TEXT, REAL & INTEGER datatypes till date.)
 
```
Usage: csv_sql inputCSVFile OutputFileName TableName field-name-1 field-name-2... 
  -v  --version  show version
  -c  --csv2sql  Converts a given csv file to a SQLite Table
  -h  --help     show this help
```
## csv_json
Convert a CSV file into JSON

```
Usage: stack exec -- csv_json [option] value fileName
  -v  --version  show version
  -k  --key      Output JSON as an array of objects keyed by a given column, KEY, rather than as a list
  -i  --indent   Indent the output JSON this many spaces. Disabled by default.
  -s  --stream   Output JSON as a stream of newline-separated objects, rather than an as an array.
  -h  --help     show this help message and exit.
```
## sql_csv
Execute an SQL query on a database and output the result to a CSV file.

```
Usage: sql_csv --db dbString --query 
  -v  --version  show version
      --query    sqlite select query to be executed
      --db       database connection url
  -h  --help     show this help
```
## intocsv
Convert common, but less awesome, tabular data formats to CSV

```
Usage: stack exec -- xls_csv [OPTION...] fileName
  -v  --version  show version
  -x  --xls-csv  Convert the xls format file to csv
  -h  --help     show help
```
